package caseStudy;

import com.ericsson.otp.erlang.*;
import detectEr.monitor.FileTracker;
import detectEr.monitor.TCPTracker;
import detectEr.monitor.TrackerFactory;

import java.io.IOException;

/**
 * Created by Gvendurst on 30.3.2017.
 */
public class client_server_java {
	public static void main(String[] args){
		if(args.length > 3) {
			new client_server_java(args);
		}
	}

	private OtpNode node = null;
	private OtpMbox mbox = null;
	private String nodeName = null;
	private String mailboxName = null;
	private static OtpErlangAtom REQUEST = new OtpErlangAtom("request");
	private static OtpErlangAtom RESPONSE = new OtpErlangAtom("response");

	private String clientTracker = "client";
	private String clientFileName = "client.txt";
	private String serverTracker = "server";
	private String serverFileName = "server.txt";
	private String monitorHost = "localhost";
	private int monitorPort = 3456;
	OtpErlangTuple clientTuple = null;



	public client_server_java(String[] args){



		if(args[2].equals("server") && args[3].equals("success")){
			setupMailbox(args[0], args[1]);
			System.out.println("Running a server that returns the correct response (Num + 1)");
			serverRoutine(args[3]);
		}
		else if(args[2].equals("server") && args[3].equals("wrong_response")){
			setupMailbox(args[0], args[1]);
			System.out.println("Running a server that returns a wrong response (Num - 1)");
			serverRoutine(args[3]);
		}
		else if(args[2].equals("server") && args[3].equals("echo")){
			setupMailbox(args[0], args[1]);
			System.out.println("Running a server that returns a wrong response (Num)");
			serverRoutine(args[3]);
		}
		else if(args[2].equals("client") && args[3].equals("success")){
			setupMailbox(args[0], args[1]);
			System.out.println("Running a client that sends a correct request");
			routine_client_success(args[4], args[5]);
		}
		else if(args[2].equals("client") && args[3].equals("no_request")){
			setupMailbox(args[0], args[1]);
			System.out.println("Running a client that sends no request");
			routine_client_no_request();
		}
		else{
			System.out.println("Unknown configuration: " + args[1] + " " + args[2]);
		}
	}

	private void setupMailbox(String nodeName, String mailboxName){
		System.out.println("nodeName: " + nodeName + ", mailboxName: " + mailboxName);
		this.nodeName = nodeName + "@" + System.getProperty("user.name");
		this.mailboxName = mailboxName;

		clientTuple = new OtpErlangTuple(new OtpErlangObject[]{new OtpErlangAtom(this.mailboxName), new OtpErlangAtom(this.nodeName)});
		//TrackerFactory.addTracker(new FileTracker(clientTuple.toString(), clientFileName));
		//TrackerFactory.addTracker(new FileTracker(serverTracker, serverFileName));
		//TrackerFactory.addTracker(new TCPTracker(serverTracker, monitorHost, monitorPort));
		TrackerFactory.addTracker(new TCPTracker(clientTuple.toString(), monitorHost, monitorPort));


		try {
			node = new OtpNode(nodeName);
			mbox = node.createMbox(mailboxName);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
	}


	private void serverRoutine(String status){
		while(true){
			OtpErlangObject message;
			OtpErlangTuple messageTuple;
			OtpErlangAtom request;
			OtpErlangPid other;
			OtpErlangLong num;

			try{
				message = mbox.receive();
				messageTuple = (OtpErlangTuple) message;
				recv(serverTracker, messageTuple.elements());

				System.out.println("Received a message: " + message);


				if(messageTuple.arity() != 3){
					System.out.println("Expected message of the form {request, Client, Num}");
				}
				else{
					request = (OtpErlangAtom)messageTuple.elementAt(0);
					other = (OtpErlangPid)messageTuple.elementAt(1);
					num = (OtpErlangLong)messageTuple.elementAt(2);

					if(request.equals(REQUEST)){
						OtpErlangObject[] reply = new OtpErlangObject[2];
						reply[0] = RESPONSE;
						reply[1] = new OtpErlangLong(getServerResponse(num.intValue(), status));

						OtpErlangTuple msg = new OtpErlangTuple(reply);
						send(serverTracker, clientTracker, reply);
						mbox.send(other, msg);
					}
					else{
						System.out.println("Expected message of the form {request, Client, Num}");
					}
				}
			}
			catch (OtpErlangExit e){
				break;
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	}

	private int getServerResponse(int num, String status){
		if(status.equals("success")){
			return num + 1;
		}
		else if(status.equals("wrong_response")){
			return num - 1;
		}
		else if(status.equals("echo")){
			return num;
		}
		else{
			return 0;
		}
	}

	public void routine_client_success(String node, String registeredName){
		System.out.println("Sending message");

		OtpErlangObject[] message = new OtpErlangObject[3];


		message[0] = REQUEST;
		message[1] = clientTuple;
		message[2] = new OtpErlangLong(1);



		OtpErlangTuple msg = new OtpErlangTuple(message);

		send(clientTuple.toString(), serverTracker, message);
		mbox.send(registeredName, node, msg);

		try {
			System.out.println("Waiting for reply");
			OtpErlangObject reply = mbox.receive();
			OtpErlangTuple replyTuple = (OtpErlangTuple) reply;

			recv(clientTuple.toString(), replyTuple.elements());

			System.out.println("Received message: " + reply);

			OtpErlangAtom response = (OtpErlangAtom) replyTuple.elementAt(0);
			OtpErlangLong num = (OtpErlangLong)replyTuple.elementAt(1);
		} catch (OtpErlangExit otpErlangExit) {
			otpErlangExit.printStackTrace();
		} catch (OtpErlangDecodeException e) {
			e.printStackTrace();
		}


	}

	public void routine_client_no_request(){

	}

	private static void send(String from, String to, Object[] objects){
		TrackerFactory.send(from, to, objects);
	}

	private static void recv(String from, Object[] objects){
		TrackerFactory.recv(from, objects);
	}
}



