package detectEr.monitor;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;


/**
 * Created by Gvendurst on 6.3.2017.
 */
//TODO: Find better names
public class TCPTracker extends ATracker{
	private static HashMap<String, TCPTracker> trackers = new HashMap<String, TCPTracker>();

	private String host;
	private int port;
	private Socket socket;
	private OutputStreamWriter outStream;

	public  TCPTracker(String name, String host, int port){
		this.name = name;
		this.host = host;
		this.port = port;
		try {
			socket = new Socket(host, port);
			outStream =new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
		}
		catch(IOException e){
			System.err.print(e.getMessage());
			e.printStackTrace();
			System.exit(-1);
		}
	}

	/*
	public static boolean setupConnection(String host, int port){
		TCPTracker.host = host;
		TCPTracker.port = port;
		try {
			socket = new Socket(host, port);
			outStream =new OutputStreamWriter(socket.getOutputStream(), "UTF-8");
		}
		catch(IOException e){
			System.err.print(e.getMessage());
			e.printStackTrace();
			return false;
		}

		//TODO: Close sockets when they are no longer being used
		//TODO: Handle it when sockets are not available

		return true;
	}
	*/
	/*
	public static void createTracker(String trackerName){
		if(trackers.containsKey(trackerName)){
			System.err.print("Tracker \"" + trackerName + "\" already exists.");
			return;
		}

		TCPTracker newTracker = new TCPTracker(trackerName);
		trackers.put(trackerName, newTracker);
	}

	public static void recv(String trackerName, Object[] params){
		if(!trackers.containsKey(trackerName)){
			System.err.println("Tracker \"" + trackerName + "\" doesn't exist");
			return;
		}

		TCPTracker t = trackers.get(trackerName);
		t.logString(t.recvMessage(params));
	}

	private String recvMessage(Object[] params){
		return "{recv," + this.trackerName + "," + toObjectList(params) + "}";
	}

	public static void send(String trackerName, String receiver, Object[] params){
		if(!trackers.containsKey(trackerName)){
			System.err.println("Tracker \"" + trackerName + "\" doesn't exist");
			return;
		}

		TCPTracker t = trackers.get(trackerName);
		t.logString(t.sendMessage(receiver, params));
	}

	private String sendMessage(String client, Object[] params){
		return "{send," + this.trackerName + "," + client + "," + toObjectList(params) + "}";
	}
	*/

	protected void logString(String theString){
		//TODO: Send via TCP

		System.out.println(theString);
		try {
			outStream.write(theString);
			outStream.flush();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	private String toObjectList(Object[] list){
		if(list.length == 0){
			return "{}";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("{" + list[0]);
		for(int i = 1; i < list.length; i++){
			sb.append(",");
			sb.append(list[i]);
		}

		sb.append("}");
		return sb.toString();
	}
	*/
}
