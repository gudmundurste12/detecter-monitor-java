package detectEr.monitor;

/**
 * Created by Gvendurst on 11.4.2017.
 */
public interface ITracker {
	void send(String to, Object[] params);
	void recv(Object[] params);
}
