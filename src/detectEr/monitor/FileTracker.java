package detectEr.monitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;


/**
 * Created by Gvendurst on 6.3.2017.
 */
//TODO: Find better names
public class FileTracker extends ATracker{
	//private static HashMap<String, FileTracker> trackers = new HashMap<String, FileTracker>();

	private String fileName;
	private boolean startedWriting = false;

	public FileTracker(String name, String fileName){
		this.name = name;
		this.fileName = fileName;

		if(fileName == ""){
			throw new IllegalArgumentException("fileName must not be empty");
		}
	}

	/*
	public static void createTracker(String trackerName, String fileName) throws IllegalArgumentException{
		if(trackers.containsKey(trackerName)){
			System.err.print("Tracker \"" + trackerName + "\" already exists.");
			return;
		}
		if(fileName == ""){
			throw new IllegalArgumentException("fileName must not be empty");
		}

		FileTracker newTracker = new FileTracker(trackerName, fileName);
		trackers.put(trackerName, newTracker);
	}

	public static void recv(String trackerName, Object[] params){
		if(!trackers.containsKey(trackerName)){
			System.err.println("Tracker \"" + trackerName + "\" doesn't exist");
			return;
		}

		FileTracker t = trackers.get(trackerName);
		t.logString(t.recvMessage(params));
	}
	*/

	/*
	private String recvMessage(Object[] params){
		return "{recv," + this.trackerName + "," + toObjectList(params) + "}.\r";
	}
	*/

	/*
	public static void send(String trackerName, String client, Object[] params){
		if(!trackers.containsKey(trackerName)){
			System.err.println("Tracker \"" + trackerName + "\" doesn't exist");
			return;
		}

		FileTracker t = trackers.get(trackerName);
		t.logString(t.sendMessage(client, params));
	}
	*/

	/*
	private String sendMessage(String client, Object[] params){
		return "{send," + this.trackerName + "," + client + "," + toObjectList(params) + "}.\r";
	}
	*/

	protected void logString(String theString){
		//TODO: Send via TCP instead?
		if(!startedWriting) {
			File outFile = new File(fileName);
			if (outFile.exists()) {
				outFile.delete();
			}
			startedWriting = true;

		}
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName, true));

			bw.write(theString + ".\r");
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*
	private String toObjectList(Object[] list){
		if(list.length == 0){
			return "{}";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("{" + list[0]);
		for(int i = 1; i < list.length; i++){
			sb.append(",");
			sb.append(list[i]);
		}

		sb.append("}");
		return sb.toString();
	}
	*/
}
