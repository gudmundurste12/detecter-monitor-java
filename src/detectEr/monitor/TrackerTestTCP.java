package detectEr.monitor;

/**
 * Created by Gvendurst on 7.3.2017.
 */
public class TrackerTestTCP {
	public static void main(String[] args){
		new TrackerTestTCP();
	}

	public TrackerTestTCP(){
		TrackerFactory.addTracker(new TCPTracker("server", "localhost", 3456));


		int sent = 1;
		TrackerFactory.send("server", "receiver", new Object[]{"request", sent});
		int received = plusOne(sent);
		TrackerFactory.recv("server", new Object[]{"response", received});
	}

	public int plusOne(int a){
		return a+1;
	}
}
