package detectEr.monitor;

import java.util.HashMap;

/**
 * Created by Gvendurst on 11.4.2017.
 */
public class TrackerFactory {
	private static HashMap<String, ATracker> trackers = new HashMap<String, ATracker>();

	public static void addTracker(ATracker theTracker){
		trackers.put(theTracker.getName(), theTracker);
	}

	public static void recv(String trackerName, Object[] params){
		if(!trackers.containsKey(trackerName)){
			System.err.println("Tracker \"" + trackerName + "\" doesn't exist");
			return;
		}

		ITracker t = trackers.get(trackerName);
		t.recv(params);
	}

	public static void send(String trackerName, String to, Object[] params){
		if(!trackers.containsKey(trackerName)){
			System.err.println("Tracker \"" + trackerName + "\" doesn't exist");
			return;
		}

		ITracker t = trackers.get(trackerName);
		t.send(to, params);
	}
}
