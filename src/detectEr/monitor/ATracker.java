package detectEr.monitor;

/**
 * Created by Gvendurst on 11.4.2017.
 */
public abstract class ATracker implements ITracker{


	protected String name;

	public String getName(){
		return name;
	}

	protected String toObjectList(Object[] list){
		if(list.length == 0){
			return "{}";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("{" + list[0]);
		for(int i = 1; i < list.length; i++){
			sb.append(",");
			sb.append(list[i]);
		}

		sb.append("}");
		return sb.toString();
	}

	protected String sendMessage(String client, Object[] params){
		return "{send," + this.name + "," + client + "," + toObjectList(params) + "}";
	}

	protected String recvMessage(Object[] params){
		return "{recv," + this.name + "," + toObjectList(params) + "}";
	}

	public void send(String to, Object[] params){
		logString(sendMessage(to, params));
	}

	public void recv(Object[] params){
		logString(recvMessage(params));
	}

	abstract void logString(String theString);
}
