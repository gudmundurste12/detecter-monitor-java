/**
 * Created by Gvendurst on 6.3.2017.
 */

import detectEr.monitor.FileTracker;
import detectEr.monitor.TrackerFactory;

public class TrackerTest {
	public static void main(String[] args){
		new TrackerTest();
	}

	public TrackerTest(){
		TrackerFactory.addTracker(new FileTracker("server", "TrackerTestServer.txt"));

		TrackerFactory.send("server", "client", new Object[]{"request", 1});
		int retVal = plusOne(1);
		TrackerFactory.recv("server", new Object[]{"response", retVal});
	}


	public int plusOne(int a){
		return a + 1;
	}
}
